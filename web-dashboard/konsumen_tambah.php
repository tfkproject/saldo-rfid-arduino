<?php
include("koneksi.php");
session_start();
if (empty($_SESSION)) {
  header("location:index.php"); // jika belum login, maka dikembalikan ke form login
}
?>
<!DOCTYPE html>
<html>
<?php include("head.php");?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>RFID</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Saldo</b>&nbsp;RFID</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <?php include("nav.php");?>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include("sidebar.php");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <!--<small>Absensi</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="dash.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <div class="col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Tambah Konsumen</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-footer">
                    <button class="btn btn-warning btn-xs" onclick="history.back(-1);"><i class="fa fa-chevron-left"></i>&nbsp;Kembali</button>
                </div>
                <?php      
                if(empty($_GET['uid'])){
                  ?>
                  <div class="box-body">
                    <center>
                      <img src="images/rfid_card.jpg" width="150px">
                      <img src="images/rfid_signal.gif" width="50px">
                      <img src="images/rc522.png" width="150px">
                      <br>
                      <br>
                      <p>Silakan tap kartu anda ke sensor RFID</p>
                    </center>
                  </div>
                  <?php
                }
                else{
				  $param = $_GET['uid'];
                  $sql = "select * from konsumen where id_konsumen = '$param'";
                  $eks = mysqli_query($koneksi, $sql);
                  $jum_row = mysqli_num_rows($eks);
                  $row = mysqli_fetch_array($eks);
                  if($jum_row == 0){
                    ?>
                    <!-- form start -->
                    <form action="konsumen_tambah_proses.php" role="form" method="POST">
                      <div class="box-body">
                        <div class="form-group">
                          <label>UID</label>
                          <input name="uid" type="hidden" value="<?php echo $param?>" >
                          <input type="text" class="form-control" placeholder="UID" value="<?php echo $param?>" disabled>
                        </div>
                        <div class="form-group">
                          <label>Nama</label>
                          <input name="nama" type="text" class="form-control" placeholder="Nama Konsumen">
                        </div>
                        <div class="form-group">
                          <label>PIN</label>
                          <input name="pin" type="number" class="form-control" placeholder="PIN Konsumen">
                        </div>
                        <div class="form-group">
                              <label>Saldo Awal</label>
                              <select name="saldo" class="form-control">
                                  <option value="10000">Rp. 10000</option>
                                  <option value="20000">Rp. 20000</option>
                                  <option value="25000">Rp. 25000</option>
                                  <option value="50000">Rp. 50000</option>
                                  <option value="100000">Rp. 100000</option>
                              </select>
                            </div>
                      </div>
                      <!-- /.box-body -->
        
                      <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </form>
                    <?php
                  }
                  else{
                    ?>
                    <script>
                      alert('Maaf kartu sudah terdaftar dengan nama <?php echo $row['nama'];?>');
                      //history.back(-1);
					  window.location = "konsumen_tambah.php";
					  window.close();
                    </script>
                    <?php
                  } 
                }
                ?>
          </div>
        </div>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include("footer.php");?>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php include("script.php");?>
</body>
</html>