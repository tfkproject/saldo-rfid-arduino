<?php
include("koneksi.php");
session_start();
if (empty($_SESSION)) {
  header("location:index.php"); // jika belum login, maka dikembalikan ke form login
}
?>
<!DOCTYPE html>
<html>
<?php include("head.php");?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>RFID</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Saldo</b>&nbsp;RFID</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <?php include("nav.php");?>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include("sidebar.php");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <!--<small>Absensi</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="dash.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <div class="col-xs-12">
          
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Pengisian Saldo</h3>
              <?php          
              if(empty($_GET['uid'])){
                ?>
                <div class="box-body">
                  <center>
                    <img src="images/rfid_card.jpg" width="150px">
                    <img src="images/rfid_signal.gif" width="50px">
                    <img src="images/rc522.png" width="150px">
                    <br>
                    <br>
                    <p>Silakan tap kartu anda ke sensor RFID</p>
                  </center>
                </div>
                <?php
              }
              else{
				$param = $_GET['uid'];    
                $sql = "select * from konsumen where id_konsumen = '$param'";
                $eks = mysqli_query($koneksi, $sql);
                $jum_row = mysqli_num_rows($eks);
                $row = mysqli_fetch_array($eks);
                if($jum_row == 0){
                  ?>
                  <script>
                    alert('Maaf sepertinya kartu tidak terdaftar');
                    //history.back(-1);
					window.location = "saldo.php";
					window.close();
                  </script>
                  <?php
                }
                ?>
                <!-- form start -->
                <form action="saldo_proses.php" role="form" method="POST">
                  <div class="box-body">
                    <div class="form-group">
                      <label>UID</label>
                      <input name="uid" type="hidden" class="form-control" value="<?php echo $param;?>">
                      <input type="text" class="form-control" placeholder="UID" value="<?php echo $param;?>" disabled>
                    </div>
                    <div class="form-group">
                      <label>Nama</label>
                      <input type="text" class="form-control" placeholder="Nama Konsumen" value="<?php echo $row['nama'];?>" disabled>
                    </div>
                    <div class="form-group">
                      <label>Saldo Konsumen</label><br>
                      <label>Rp.&nbsp;</label><label><?php echo $row['saldo'];?></label>
                    </div>
                    <div class="form-group">
                          <label>Tambah Saldo</label>
                          <select id="jenis" class="form-control" onchange="bacaHasil();">
                              <option value="0">Pilih</option>
                              <option value="10000">Rp. 10000</option>
                              <option value="20000">Rp. 20000</option>
                              <option value="25000">Rp. 25000</option>
                              <option value="50000">Rp. 50000</option>
                              <option value="100000">Rp. 100000</option>
                          </select>
                        </div>
                    <div class="form-group">
                      <label>Saldo Konsumen Akan Menjadi:</label><br>
                      <label>Rp.&nbsp;</label><input type="text" name="saldo" value="" id="sal" style="color: green" id="sal" placeholder="Silakan pilih saldo">
                    </div>
                    <script>
                    function bacaHasil() {
                        var x = document.getElementById("jenis").value;
                        var y = <?php echo $row['saldo'];?>;
                        var h = parseInt(x) + parseInt(y);
                        document.getElementById("sal").value = h;
                    }
                    </script>
                  <!-- /.box-body -->
    
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
                <?php
              }
              ?>
            </div>
            
            
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include("footer.php");?>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php include("script.php");?>
</body>
</html>