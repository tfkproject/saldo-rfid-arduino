<?php
session_start();
if (empty($_SESSION)) {
  header("location:index.php"); // jika belum login, maka dikembalikan ke form login
}
?>
<!DOCTYPE html>
<html>
<?php include("head.php");?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>RFID</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Saldo</b>&nbsp;RFID</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <?php include("nav.php");?>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include("sidebar.php");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <!--<small>Absensi</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="dash.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
                <?php
                $sql = "SELECT COUNT(*) as total_konsumen FROM `konsumen`";
                $eks = mysqli_query($koneksi, $sql);
                $row = mysqli_fetch_array($eks);
                ?>
              <h3><?php echo $row['total_konsumen'];?></h3>

              <p><b>Total Konsumen Terdaftar</b></p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
            <a href="konsumen.php" class="small-box-footer">Lihat data <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>Isi saldo<!--<sup style="font-size: 20px">%</sup>--></h3>

              <p>Pengisian Saldo</p>
            </div>
            <div class="icon">
              <i class="fa fa-credit-card"></i>
            </div>
            <a href="saldo.php" class="small-box-footer">Pilih <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>Pembayaran</h3>

              <p>Pembayaran Minyak</p>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
            <a href="pembayaran.php" class="small-box-footer">Pilih <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <!--<div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <!--<div class="small-box bg-purple">
            <div class="inner">
                <?php
                $sql = "SELECT COUNT(*) as total_agenda FROM `agenda` WHERE `tanggal` < CURDATE()";
                $eks = mysqli_query($koneksi, $sql);
                $row = mysqli_fetch_array($eks);
                ?>
              <h3><?php echo $row['total_agenda'];?></h3>

              <p>Agenda Kemarin</p>
            </div>
            <div class="icon">
              <i class="fa fa-calendar-minus-o"></i>
            </div>
            <a href="agenda.php?lihat=yesterday" class="small-box-footer">Lihat data <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>-->
        <!-- ./col -->
      <!--</div>-->
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
            <!-- Sisi kiri -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">
            <!-- Sisi kanan -->
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include("footer.php");?>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php include("script.php");?>
</body>
</html>
