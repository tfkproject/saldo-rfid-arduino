<?php
include("koneksi.php");
session_start();
if (empty($_SESSION)) {
  header("location:index.php"); // jika belum login, maka dikembalikan ke form login
}
?>
<!DOCTYPE html>
<html>
<?php include("head.php");?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>RFID</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Saldo</b>&nbsp;RFID</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <?php include("nav.php");?>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include("sidebar.php");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <!--<small>Absensi</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="dash.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <div class="col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Konsumen</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-footer">
                    <button class="btn btn-warning btn-xs" onclick="history.back(-1);"><i class="fa fa-chevron-left"></i>&nbsp;Kembali</button>
                </div>
                <?php
                $id = $_GET['id'];
                
                $sql = "select * from konsumen where id_konsumen = '$id'";
                $eks = mysqli_query($koneksi, $sql);
                $row = mysqli_fetch_array($eks);
                ?>
                <!-- form start -->
                <form action="konsumen_edit_proses.php" role="form" method="POST">
                  <div class="box-body">
                    <div class="form-group">
                      <label>Nama</label>
                      <input name="id_konsumen" type="hidden" class="form-control" value="<?php echo $row['id_konsumen'];?>">
                      <input name="nama" type="text" class="form-control" placeholder="Nama Konsumen" value="<?php echo $row['nama'];?>">
                    </div>
                    <div class="form-group">
                      <label>PIN</label>
                      <input name="pin" type="text" class="form-control" placeholder="PIN Konsumen" value="<?php echo $row['pin'];?>">
                    </div>
                    
                  </div>

                  <!-- /.box-body -->
    
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>
                </form>
          </div>
        </div>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include("footer.php");?>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php include("script.php");?>
</body>
</html>