<?php
include("koneksi.php");
session_start();
if (empty($_SESSION)) {
  header("location:index.php"); // jika belum login, maka dikembalikan ke form login
}
?>
<!DOCTYPE html>
<html>
<?php include("head.php");?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>RFID</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Saldo</b>&nbsp;RFID</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <?php include("nav.php");?>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include("sidebar.php");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <!--<small>Absensi</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="dash.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <div class="col-xs-12">
          
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Konsumen</h3>
            </div>
            <div class="box-footer">
                <a href="konsumen_tambah.php"><button class="btn btn-success btn-xs"><i class="fa fa-plus"></i>&nbsp;Tambah</button></a>
            </div>
            
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>ID Kartu</th>
                  <th>Nama</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                $sql = "select * from konsumen order by nama asc";
                $eks = mysqli_query($koneksi, $sql);
                while($row = mysqli_fetch_array($eks)){
                ?>
                <tr>
                  <td><?php echo $no++;?></td>
                  <td><?php echo $row['id_konsumen'];?></td>
                  <td><?php echo $row['nama'];?></td>
                  <td>
                      <a href="konsumen_edit.php?id=<?php echo $row['id_konsumen'];?>"><button class="btn btn-success btn-xs"><i class="fa fa-pencil"></i></button></a>&nbsp;<a href="konsumen_hapus.php?id=<?php echo $row['id_konsumen'];?>" onClick="return confirm('Yakin ingin menghapus data?');"><button class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button></a>
                  </td>
                </tr>
                <?php
                }
                ?>
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include("footer.php");?>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php include("script.php");?>
</body>
</html>