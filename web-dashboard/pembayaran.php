<!DOCTYPE html>
<html>
<?php
include("head.php");
include("koneksi.php");
?>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <b>Saldo</b>&nbsp;RFID
    <hr>
    <img src="images/arduino.png" width="120px">
    <i class="fa fa-plus"></i>
    <img src="images/rc522.png" width="120px">
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <?php
    /**
    * Pesan Error Bila terjadi kegagalan dalam proses
    */
    if (isset($_GET['bayar']) && $_GET['bayar'] == 'berhasil' && isset($_GET['saldo'])) {
      echo '<div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          Saldo anda berhasil di potong.
          <br>
          Sisa saldo anda sekarang Rp. '.$_GET['saldo'].'
        </div>'; 
    }
    if (isset($_GET['bayar']) && $_GET['bayar'] == 'gagal' && isset($_GET['saldo'])) {
      echo '<div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          Maaf, saldo anda tidak cukup.
          <br>
          Saldo anda Rp. '.$_GET['saldo'].'
        </div>'; 
    }
    if (isset($_GET['masuk']) && $_GET['masuk'] == 'salah') {
      echo '<div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          Maaf PIN salah!
        </div>'; 
    }
    if (isset($_GET['masuk']) && $_GET['masuk'] == 'error') {
      echo '<div class="alert alert-warning alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          PIN tidak boleh kosong!
        </div>'; 
    }
            
    if(empty($_GET['uid'])){
      ?>
      <p class="login-box-msg">Silakan tap kartu anda untuk melakukan pembayaran</p>
      <hr>
      <?php
    }
    else{
      $param = $_GET['uid'];  
      $sql = "select * from konsumen where id_konsumen = '$param'";
      $eks = mysqli_query($koneksi, $sql);
      $jum_row = mysqli_num_rows($eks);
      $row = mysqli_fetch_array($eks);
      if($jum_row == 0){
      ?>
      <script>
        alert('Maaf sepertinya kartu tidak terdaftar');
        //history.back(-1);
		window.location = "pembayaran.php";
		window.close();
      </script>
      <?php
      }
      else{
        $potong = $_GET['potong'];
        ?>
        <p class="login-box-msg"><b>Masukkan PIN anda</b> untuk proses pembayaran</p>
        <form action="pembayaran_proses.php" method="GET">
          <div class="form-group has-feedback">
            <input name="uid" type="hidden" class="form-control" value="<?php echo $param;?>">
            <input name="potong" type="hidden" class="form-control" value="<?php echo $potong;?>">
            <input name="nama" type="text" class="form-control" placeholder="Nama User" value="<?php echo $row['nama'];?>" disabled="">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input name="pin" type="password" class="form-control" placeholder="PIN">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
          
            <!-- /.col -->
            <div class="col-xs-12"><!--<div class="col-xs-4">-->
              <button type="submit" class="btn btn-primary btn-block btn-flat">Bayar</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
        <?php
      }
      
    }
    
	?>
    
    
    <br>
    <a href="dash.php" class="text-center"><i class="fa fa-arrow-left"></i>&nbsp;Kembali ke dashboard</a>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<?php include("script.php");?>
</body>
</html>
