<!DOCTYPE html>
<html>
<?php include("head.php");?>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <b>Saldo</b>&nbsp;RFID
    <br><sup style="font-size: 20px">Administrator</sup>
    <hr>
    <img src="images/arduino.png" width="120px">
    <i class="fa fa-plus"></i>
    <img src="images/rc522.png" width="120px">
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Login untuk masuk ke dashboard</p>
    
    <?php 
		/**
		* Pesan Error Bila terjadi kegagalan dalam login
		*/
		if (isset($_GET['login']) && $_GET['login'] == 'salah') {
			echo '<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Maaf email atau password salah!
				</div>'; 
		}
		if (isset($_GET['login']) && $_GET['login'] == 'error') {
			echo '<div class="alert alert-warning alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Email atau password tidak boleh kosong!
				</div>'; 
		}
	?>
    
    <form action="login.php" method="post">
      <div class="form-group has-feedback">
        <input name="email" type="text" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input name="password" type="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
      
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Log In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<?php include("script.php");?>
</body>
</html>
